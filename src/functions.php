<?php
namespace Spinit\UUIDO;

/* 
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

/**
 * Generatore codici esadecimali random
 * 
 * @param int $len lunghezza codice
 * @return hex
 */
function randHex($len = 16)
{
    $str = dechex(rand(1,15));
    $len -= strlen($str);
    while ($len-- > 0) {
        $str .= dechex(rand(0,15));
    }
    return $str;
}

function randHexToday($len = 10)
{
    $frm = explode(',',date('Y,z'));
    // L'anno occupa i primi 2 esadecimali quindi gestisce 16*16 anni.
    // Siccome i giorni sono non più di 400 allora di possono gestire gli anni pari diversamente dagli anni dispari
    // operando direttamente sui giorni. Quindi gli anni pari anno i giorni > 400 mentre quelli dispare < 400.
    // In questo modo è possibile gestire 16*16*2 anni ... abbastanza penso
    
    $mod = $frm[0] % 2;
    $ret  = str_pad(substr(dechex(($frm[0] - 2000 - $mod) / 2), 0, 2), 2, '0', STR_PAD_LEFT);
    $ret .= str_pad($frm[1] + ($mod?500:0), 3, '0', STR_PAD_LEFT);
    $len -= strlen($ret);
    while ($len-- > 0) {
        $ret .= dechex(rand(0,15));
    }
    return strtoupper($ret);
}
/**
 * Verifica se la stringa è esadecimale ed effettua un padding a sinistra
 * @param type $hex
 * @param type $len
 * @return type
 * @throws \Exception
 */
function check($hex, $len = 0)
{
    if (! ctype_xdigit($hex ? $hex : '0')) {
        throw new \Exception('Codice non esadecimale : '.$hex);
    }
    if ($len and strlen($hex) > $len) {
        throw new \Exception('Lunghezza codice maggiore di '.$len.' : '.$hex);
    }
    return strtoupper(str_pad($hex, $len, '0', STR_PAD_LEFT));
}
