<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\UUIDO;

/**
 * Description of OuidTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MakerTest  extends \PHPUnit_Framework_TestCase
{
    private $object;
    
    public function testValues()
    {
        $node = randHex();
        $this->assertEquals(16, strlen($node));
        $this->object = new Maker($node);
        
        $this->object->next();
        $this->assertEquals(0, $this->object->key());
        $value1 = $this->object->current();
        $this->assertEquals(32, strlen($value1));
        
        // si porta il contatore all'ultimo ID utile
        $this->object->getCounter()->init(hexdec('fffffff'));
        $this->object->next();
        $this->assertEquals(1, $this->object->key());
        $value2 = $this->object->current();
        $this->assertEquals(32, strlen($value2));
        
        // le radici dei due valori devono essere diversi
        $this->assertNotEquals(substr((string)$value1, 0, 14), substr((string)$value2, 0, 14));
        
        $value3 = '';
        // il generatore viene riavviato
        foreach($this->object as $key => $value) {
            $this->assertEquals(0, $key);
            $value3 = $value;
            break;
        }
        
        // le radici dei valori devono essere diversi
        $this->assertNotEquals(substr($value1, 0, 14), substr($value3, 0, 14));
        $this->assertNotEquals(substr($value2, 0, 14), substr($value3, 0, 14));
        
        $this->assertEquals(substr($value3, 0, 14), substr($value3->next(), 0, 14));
    }
    
    /**
     * @expectedException \Exception
     */
    public function testNodeBad()
    {
        check('hello world');
    }
    
    /**
     * @expectedException \Exception
     */
    public function testNodeTooLong()
    {
        check('123456789ABCDEF01', 10);
    }
    
    public function testNext()
    {
        $this->object = new Maker(randHexToday());
        $this->assertEquals(strlen($this->object->next()), strlen($this->object->next()));
        //echo $this->object->next();
    }
}
